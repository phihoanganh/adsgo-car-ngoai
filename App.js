
import React, { Component } from "react";
import { StyleSheet, BackHandler, SafeAreaView } from "react-native";
import { Provider, connect } from "react-redux";
import Orientation from 'react-native-orientation';
import { StackNavigator, addNavigationHelpers, NavigationActions } from "react-navigation";
// import { BackHandler } from "react-native";
import routes from './src/views/route'
import getStore from "./store";
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition';
// import NavigationExperimental from 'react-native-deprecated-custom-components';

const AppNavigator = StackNavigator(routes, {
    initialRouteName: 'splash',
    headerMode: 'none',
    transitionConfig: getSlideFromRightTransition,
    navigationOptions: {
        gesturesEnabled: false
    }
});


const navReducer = (state, action) => {
    const newState = AppNavigator.router.getStateForAction(action, state);
    return newState || state;
};

class Views extends Component {
    render() {
        return (
            <AppNavigator
                navigation={addNavigationHelpers({
                    dispatch: this.props.dispatch,
                    state: this.props.nav
                })}
            />
        );
    }
    componentDidMount() {
        Orientation.lockToPortrait();
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress = () => {
        const { dispatch, nav } = this.props;
        if (nav.index === 0) {
            return true;
        }
        dispatch(NavigationActions.back());
        return true;
    };
}
const mapStateToProps = (state) => ({
    nav: state.nav
});

const AppWithNavigationState = connect(mapStateToProps)(Views);

const store = getStore(navReducer);

export default class App extends Component {
    render() {
        return (
            <SafeAreaView style={styles.safeArea}>
                <Provider store={store}>
                    <AppWithNavigationState />
                </Provider>
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#ddd'
    }
})