import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'

export default Object.freeze({
    getReportList: function (body, callback) {
        NetworkUtils.get(null, ServerPath.get_report_list_api, body, callback);
    }
});
