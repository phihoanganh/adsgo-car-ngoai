import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'

export default Object.freeze({
    uploadFile: function (body, fileData, callback) {
        NetworkUtils.uploadFile(null, ServerPath.upfile_api, body, fileData, callback);
    }
});
