import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'

export default Object.freeze({
    getCampaignList: function (body, callback) {
        NetworkUtils.getparam(null, ServerPath.get_campaign_list_api, body, callback);
    }
});
