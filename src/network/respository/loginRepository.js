import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'

export default Object.freeze({
    doLogin: function (body, callback) {
        NetworkUtils.postWtToken(null, ServerPath.login_api, body, callback);
    },
    getUserInfo: function (body, callback) {
        NetworkUtils.getparam(null, ServerPath.get_user_info_api, body, callback);
    }
});
