import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'

export default Object.freeze({
    sendRequestSp: function (body, callback) {
        NetworkUtils.post(null, ServerPath.send_request_sp_api, body, callback);
    }
});