import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'

export default Object.freeze({
    getImagesRequest: function (body, callback) {
        NetworkUtils.get(null, ServerPath.get_list_images_request_api, body, callback);
    }
});
