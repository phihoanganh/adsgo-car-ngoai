import ServerPath from '../net/ServerPath'
import NetworkUtils from '../net/NetworkUtils'

export default Object.freeze({
    changePass: function (body, callback) {
        NetworkUtils.put(null, ServerPath.change_pass_api, body, callback);
    },
    logout: function (callback) {
        NetworkUtils.get(null, ServerPath.logout_api, null, callback);
    }
});
