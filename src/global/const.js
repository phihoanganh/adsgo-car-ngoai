export default Object.freeze({
    tab_menu: [{
        name: 'Trang chủ',
        route: 'home',
        data: null,
        img: require('../assets/images/icon_home.png')
    },
    {
        name: 'Hỗ trợ',
        route: 'support',
        data: null,
        img: require('../assets/images/icon_support.png')
    },
    {
        name: 'Chiến dịch',
        route: 'campaign_his',
        data: null,
        img: require('../assets/images/icon_campaign.png')
    },
    {
        name: 'Tài khoản',
        route: 'user',
        data: null,
        img: require('../assets/images/icon_account.png')
    }],
    SUCCESS: 'SUCCESS',
    FAILURE: 'FAILURE',
    ACCESS_TOKEN_STORE_ID: 'ACCESS_TOKEN_STORE_ID',
    LOGIN_INPUT_STORE_ID: 'LOGIN_INPUT_STORE_ID',
    NETOWRK_ERROR_TITLE: 'Không thể kết nối mạng',
    USER_INFO: 'USER_INFO',
    NETOWRK_ERROR_MESSAGE: 'Không có kết nối mạng hoặc mất kết nối đến server.',
    TOKEN_ERROR_TITLE: 'Không thể kết nối tài khoản',
    TOKEN_ERROR_MESSAGE: 'Không thể kết nối với tài khoản, vui lòng đăng nhập lại.',
    LOGIN_FAIL_TITLE: 'Đăng nhập không thành công',
    LOGIN_FAIL_MESSAGE: 'Tên đăng nhập hoặc mật khẩu không chính xác! Vui lòng thử lại',
    LOGIN_OTHER_PLACE_MESSAGE: 'Tài khoản hiện đang đăng nhập trên thiết bị khác',
    REQUEST_ERROR_TITLE: 'Thông báo',
    NOT_ENOUGH_IMG_ERROR_MESSAGE: 'Vui lòng chụp đủ hình trước khi gửi',
    REQUEST_ERROR_MESSAGE: 'Có lỗi xảy ra! Vui lòng thử lại',
    NOTIFY_TITLE: 'Thông báo',
    MAX_LENGTH_20_ERROR: 'Vui lòng nhập ít nhất 20 ký tự',
    CHANGE_PASS_SUCCESS: 'Đổi mật khẩu thành công',
    OLD_PASS_WRONG_MESSAGE: 'Mật khẩu cũ không chính xác',

});
