import HomeRepository from '../../network/respository/homeRepository';
import RequestReportActionType from '../../action_type/data_type/requestReportActionType'
import Const from '../../global/const'
import Utils from '../../global/utils'

export function getUserInfoSuccess(data) {
    return {
        type: RequestReportActionType.GET_REQUEST_REPORT_SUCCESS,
        data
    };
}

export function getImagesRequest(context) {
    return dispatch => {
        Utils.getValueByKey(Const.LOGIN_INPUT_STORE_ID).then((result) => {
            if (result !== '' && result !== undefined && result !== null) {
                result = JSON.parse(result);
                HomeRepository.getImagesRequest({
                    email: result.username
                }, function (status, responseJson) {
                    console.log(responseJson);
                    context.setLoading(false);
                    if (status == Const.SUCCESS && responseJson.status == 200) {
                        if (responseJson.json.length > 0) {
                            dispatch(getUserInfoSuccess(responseJson.json[0]));
                            context.openRequestImagePopup();
                        }
                    }
                });
            } else {
                context.setLoading(false);
            }
        });
    };
}
