import ReportRepository from '../../network/respository/reportRepository';
import UpfileRepository from '../../network/respository/upfileRepository';
import Const from '../../global/const'

export function sendReport(id, email, licensePlate, imageSources, context) {
    return dispatch => {
        context.setLoading(true);
        let imageSourcesTemp = [];
        let imageSourcesString = '';
        imageSources.forEach(function (element) {
            if (element) {
                imageSourcesTemp.push(element);
                imageSourcesString += element.getParts()[1].name + ',';
            }
        });
        if (imageSourcesTemp.length <= 0) return;
        imageSourcesString = imageSourcesString.slice(0, -1);
        dispatch(uploadImage(id, email, licensePlate, imageSourcesTemp, 0, imageSourcesString, context));
    };
}

export function uploadImage(id, email, licensePlate, imageSources, index, imageSourcesString, context) {
    return dispatch => {
        console.log('send RQ');
        UpfileRepository.uploadFile({
            email: email
        }, imageSources[index],
            function (status, responseJson) {
                console.log(responseJson)
                if (status == Const.SUCCESS) {
                    if (responseJson.status == 200) {
                        if (imageSources.length <= index + 1) {
                            dispatch(sendReportImg(id, licensePlate, imageSourcesString, context));
                        } else {
                            index++;
                            dispatch(uploadImage(id, email, licensePlate, imageSources, index, imageSourcesString, context));
                        }
                    } else {
                        context.setLoading(false);
                        context.renderPopup(
                            Const.REQUEST_ERROR_TITLE,
                            Const.REQUEST_ERROR_MESSAGE,
                            require('../../assets/images/icon_fail.png'),
                            null,
                            2,
                            null
                        );
                    }
                } else {
                    context.setLoading(false);
                    context.renderPopup(
                        Const.NETOWRK_ERROR_TITLE,
                        Const.NETOWRK_ERROR_MESSAGE,
                        require('../../assets/images/icon_fail.png'),
                        null,
                        2,
                        null
                    );
                }
            });
    };
}


export function sendReportImg(id, licensePlate, image, context) {
    return dispatch => {
        // let { setLoading } = context.props.screenProps;
        console.log(image);
        ReportRepository.sendReport({
            bienKiemSoats: licensePlate,
            id: id,
            label: "Đã chụp",
            trangTraiReport: "Chưa duyệt",
            urlReport: image
        }, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.status == 200) {
                    context.onSendSuccess();
                } else {
                    context.renderPopup(
                        Const.REQUEST_ERROR_TITLE,
                        Const.REQUEST_ERROR_MESSAGE,
                        require('../../assets/images/icon_fail.png'),
                        null,
                        2,
                        null
                    );
                }
            } else {
                context.renderPopup(
                    Const.NETOWRK_ERROR_TITLE,
                    Const.NETOWRK_ERROR_MESSAGE,
                    require('../../assets/images/icon_fail.png'),
                    null,
                    2,
                    null
                );
            }
        });
    };
}
