import SupportRepository from '../../network/respository/supportRepository';
import UpfileRepository from '../../network/respository/upfileRepository';
import Const from '../../global/const'

export function sendRequestSp(email, imageSources, message, context) {
    return dispatch => {
        context.setLoading(true);
        let imageSourcesTemp = [];
        let imageSourcesString = '';
        imageSources.forEach(function (element) {
            if (element) {
                imageSourcesTemp.push(element);
                imageSourcesString += element.getParts()[1].name + ',';
            }
        });
        if (imageSourcesTemp.length > 0) {
            imageSourcesString = imageSourcesString.slice(0, -1);
            dispatch(uploadImage(email, imageSourcesTemp, 0, message, imageSourcesString, context));
        } else {
            dispatch(sendRequest(email, null, message, context));
        }
    };
}

export function uploadImage(email, imageSources, index, message, imageSourcesString, context) {
    return dispatch => {
        console.log('send RQ');
        UpfileRepository.uploadFile({
            email: email
        }, imageSources[index],
            function (status, responseJson) {
                console.log(responseJson)
                if (status == Const.SUCCESS) {
                    if (responseJson.status == 200) {
                        if (imageSources.length <= index + 1) {
                            dispatch(sendRequest(email, imageSourcesString, message, context));
                        } else {
                            index++;
                            dispatch(uploadImage(email, imageSources, index, message, imageSourcesString, context));
                        }
                    } else {
                        context.setLoading(false);
                        context.renderPopup(
                            Const.REQUEST_ERROR_TITLE,
                            Const.REQUEST_ERROR_MESSAGE,
                            require('../../assets/images/icon_fail.png'),
                            null,
                            2,
                            null
                        );
                    }
                } else {
                    context.setLoading(false);
                    context.renderPopup(
                        Const.NETOWRK_ERROR_TITLE,
                        Const.NETOWRK_ERROR_MESSAGE,
                        require('../../assets/images/icon_fail.png'),
                        null,
                        2,
                        null
                    );
                }
            });
    };
}


export function sendRequest(email, image, message, context) {
    return dispatch => {
        // let { setLoading } = context.props.screenProps;
        console.log(image);
        SupportRepository.sendRequestSp({
            email: email,
            image: image,
            message: message
        }, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.status == 200) {
                    context.onSendSuccess();
                } else {
                    context.renderPopup(
                        Const.REQUEST_ERROR_TITLE,
                        Const.REQUEST_ERROR_MESSAGE,
                        require('../../assets/images/icon_fail.png'),
                        null,
                        2,
                        null
                    );
                }
            } else {
                context.renderPopup(
                    Const.NETOWRK_ERROR_TITLE,
                    Const.NETOWRK_ERROR_MESSAGE,
                    require('../../assets/images/icon_fail.png'),
                    null,
                    2,
                    null
                );
            }
        });
    };
}
