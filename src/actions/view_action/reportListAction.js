import ReportListRepository from '../../network/respository/reportListRepository';
import { ReportListActionType } from '../../action_type';
import Const from '../../global/const';

export function getReportSuccess(data) {
    return {
        type: ReportListActionType.GET_REPORT_LIST_SUCCESS,
        data
    };
}

export function getReportFail() {
    return {
        type: ReportListActionType.GET_REPORT_LIST_FAIL
    };
}

export function getReportList(email, context) {
    return dispatch => {
        context.setLoading(true);
        ReportListRepository.getReportList({
            email: email
        }, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.status == 200) {
                    dispatch(getReportSuccess(responseJson.json));
                    // context.onSendSuccess();
                } else {
                    dispatch(getReportFail());
                }
            } else {
                dispatch(getReportFail());
            }
        });
    };
}
