import CampaignRepository from '../../network/respository/campaignRepository';
import { CampaignActionType } from '../../action_type';
import Const from '../../global/const';

export function getCampaignSuccess(data) {
    return {
        type: CampaignActionType.GET_CAMPAIGN_HIS_SUCCESS,
        data
    };
}

export function getCampaignFail() {
    return {
        type: CampaignActionType.GET_CAMPAIGN_HIS_FAIL
    };
}

export function getCampaignList(email, context) {
    return dispatch => {
        context.setLoading(true);
        CampaignRepository.getCampaignList({
            email: email
        }, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.status == 200) {
                    dispatch(getCampaignSuccess(responseJson.json));
                    // context.onSendSuccess();
                } else {
                    dispatch(getCampaignFail());
                }
            } else {
                dispatch(getCampaignFail());
            }
        });
    };
}
