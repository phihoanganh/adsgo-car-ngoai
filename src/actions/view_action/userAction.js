import LoginRepository from '../../network/respository/loginRepository';
import UserRepository from '../../network/respository/userRepository';
import { LoginActionType } from '../../action_type'
import Const from '../../global/const'
import Utils from '../../global/utils'

export function getUserInfoSuccess(data) {
    return {
        type: LoginActionType.GET_USER_INFO_SUCCESS,
        data
    };
}

export function getUserInfo(cmnd, context) {
    return dispatch => {
        context.setLoading(true);
        LoginRepository.getUserInfo({
            cmnd: cmnd
        }, function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS && responseJson.status == 200) {
                dispatch(getUserInfoSuccess(responseJson.json));
                Utils.setValueByKey(Const.USER_INFO, JSON.stringify(responseJson.json));
            } else {
                context.renderPopup(
                    Const.TOKEN_ERROR_TITLE,
                    Const.TOKEN_ERROR_MESSAGE,
                    require('../../assets/images/icon_fail.png'),
                    null,
                    2,
                    null
                );
            }
        });
    }
}

export function changePass(email, oldPass, newPass, context) {
    return dispatch => {
        context.changingPass(true);
        UserRepository.changePass({
            newPassword: newPass,
            oldPassword: oldPass
        }, function (status, responseJson) {
            context.changingPass(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.status == 200) {
                    Utils.setValueByKey(Const.LOGIN_INPUT_STORE_ID, JSON.stringify({ username: email, password: newPass }));
                    context.setState({ modalVisible: false })
                    context.renderPopup(
                        Const.NOTIFY_TITLE,
                        Const.CHANGE_PASS_SUCCESS,
                        null,
                        null,
                        1,
                        null);
                } else if (responseJson.status == 400) {
                    context.renderPopup(
                        Const.REQUEST_ERROR_TITLE,
                        Const.OLD_PASS_WRONG_MESSAGE,
                        require('../../assets/images/icon_fail.png'),
                        null,
                        2,
                        null);
                } else {
                    context.renderPopup(
                        Const.REQUEST_ERROR_TITLE,
                        Const.REQUEST_ERROR_MESSAGE,
                        require('../../assets/images/icon_fail.png'),
                        null,
                        2,
                        null
                    );
                }
            } else {
                context.renderPopup(
                    Const.NETOWRK_ERROR_TITLE,
                    Const.NETOWRK_ERROR_MESSAGE,
                    require('../../assets/images/icon_fail.png'),
                    null,
                    2,
                    null
                );
            }
        });
    };
}

export function logout(username, context) {
    return dispatch => {
        context.setLoading(true);
        UserRepository.logout(function (status, responseJson) {
            context.setLoading(false);
            console.log(responseJson)
            if (status == Const.SUCCESS) {
                if (responseJson.status == 200) {
                    Utils.setValueByKey(Const.ACCESS_TOKEN_STORE_ID, '');
                    Utils.setValueByKey(Const.LOGIN_INPUT_STORE_ID, JSON.stringify({ username: username, password: '' }));
                    context.resetRootPage(0, 'login', null)
                } else {
                    context.renderPopup(
                        Const.REQUEST_ERROR_TITLE,
                        Const.REQUEST_ERROR_MESSAGE,
                        require('../../assets/images/icon_fail.png'),
                        null,
                        2,
                        null
                    );
                }
            } else {
                context.renderPopup(
                    Const.NETOWRK_ERROR_TITLE,
                    Const.NETOWRK_ERROR_MESSAGE,
                    require('../../assets/images/icon_fail.png'),
                    null,
                    2,
                    null
                );
            }
        });
    };
}
