import ReportListActionType from '../../action_type/view_type/reportListActionType'

const reportListReducer = (state = ReportListActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case ReportListActionType.GET_REPORT_LIST_SUCCESS:
            return {
                ...state,
                report_list: action.data,
                loadDone: true,
                get_report_list_error: false
            };
        case ReportListActionType.GET_REPORT_LIST_SUCCESS:
            return {
                ...state,
                loadDone: false,
                get_report_list_error: true
            };
        default:
            return state;
    }
};

export default reportListReducer;