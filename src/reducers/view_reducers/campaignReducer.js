import CampaignActionType from '../../action_type/view_type/campaignActionType'

const campaignReducer = (state = CampaignActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case CampaignActionType.GET_CAMPAIGN_HIS_SUCCESS:
            return {
                ...state,
                campaign_list: action.data,
                loadDone: true,
                get_campaign_error: false
            };
        case CampaignActionType.GET_CAMPAIGN_HIS_FAIL:
            return {
                ...state,
                loadDone: false,
                get_campaign_error: true
            };
        default:
            return state;
    }
};

export default campaignReducer;