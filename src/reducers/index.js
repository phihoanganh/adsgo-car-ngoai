import { combineReducers } from 'redux';
import splashReducer from './SplashReducer';
import loginReducer from './view_reducers/loginReducer'
import campaignReducer from './view_reducers/campaignReducer'
import reportListReducer from './view_reducers/reportListReducer'
import requestReportReducer from './data_reducers/requestReportReducer'




export default function getRootReducer(navReducer) {
    return combineReducers({
        nav: navReducer,
        splashPage: splashReducer,
        loginPage: loginReducer,
        campaignPage: campaignReducer,
        reportListPage: reportListReducer,
        requestReportData: requestReportReducer
    });
}
