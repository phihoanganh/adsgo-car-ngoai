import RequestReportActionType from '../../action_type/data_type/requestReportActionType'

const requestReportReducer = (state = RequestReportActionType.DEFAULT_STATE, action) => {
    switch (action.type) {
        case RequestReportActionType.GET_REQUEST_REPORT_SUCCESS:
            return {
                ...state,
                request_list: action.data,
            };
        default:
            return state;
    }
};

export default requestReportReducer;