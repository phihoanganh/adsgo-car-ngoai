// import {SplashActionType} from 'action_type'

const splashReducer = (state = { error: false}, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default splashReducer;