export default Object.freeze({
    GET_CAMPAIGN_HIS_SUCCESS: 'GET_CAMPAIGN_HIS_SUCCESS',
    GET_CAMPAIGN_HIS_FAIL: 'GET_CAMPAIGN_HIS_FAIL',
    DEFAULT_STATE: {
        get_campaign_error: false,
        campaign_list: [],
        loadDone: false
    }
});