export default Object.freeze({
    GET_REPORT_LIST_SUCCESS: 'GET_REPORT_LIST_SUCCESS',
    GET_REPORT_LIST_FAIL: 'GET_REPORT_LIST_FAIL',
    DEFAULT_STATE: {
        get_report_list_error: false,
        report_list: [],
        loadDone: false
    }
});