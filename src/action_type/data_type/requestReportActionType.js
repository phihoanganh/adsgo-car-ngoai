export default Object.freeze({
    GET_REQUEST_REPORT_SUCCESS: 'GET_REQUEST_REPORT_SUCCESS',
    GET_REQUEST_REPORT_FAIL: 'GET_REQUEST_REPORT_FAIL',
    DEFAULT_STATE: {
        request_list: []
    }
});