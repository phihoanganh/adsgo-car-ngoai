import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Modal,
    Image,
    TextInput
} from 'react-native';
import Strings from '../../global/strings';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class InputRow extends Component {
    static propTypes = {
        onResponse: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false
        }
    }

    render() {
        return (
            <Modal
                visible={this.state.modalVisible}
                onRequestClose={() => this.setState({ modalVisible: false })}
                transparent={true}>
                <TouchableWithoutFeedback onPress={() => this.setState({ modalVisible: false })} >
                    <View style={styles.modal_box}>
                        <TouchableWithoutFeedback>
                            <View style={styles.popup_box}>
                                <View style={styles.title_box}>
                                    <Text style={styles.title_text}>Nhập biển số xe</Text>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ modalVisible: false })}
                                        style={styles.icon_cancel}
                                        underlayColor='transparent'>
                                        <Image style={styles.icon} resizeMode={'contain'} source={require('../../assets/images/icon_cancel.png')}></Image>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.box_btn}>
                                    <TextInput
                                        underlineColorAndroid={'transparent'}
                                        placeholder="Nhập biển số xe"
                                        style={styles.plate_input} />
                                    <TouchableOpacity
                                        onPress={() => { }}
                                        style={styles.submit}
                                        underlayColor='transparent'>
                                        <Text style={styles.btn_text}>{Strings.confirm}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        );

    }
    openPopup() {
        this.setState({ modalVisible: true })
    }
}
