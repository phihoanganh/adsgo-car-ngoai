import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Modal
} from 'react-native';
import Strings from '../../global/strings';

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class InputRow extends Component {
    static propTypes = {
        onResponse: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            dateRequest: null
        }
    }

    render() {
        return (
            <Modal
                visible={this.state.modalVisible}
                onRequestClose={() => this.setState({ modalVisible: true })}
                transparent={true}>
                <View style={styles.modal_box}>
                    <TouchableWithoutFeedback>
                        <View style={styles.popup_box}>
                            <View style={styles.title_box}>
                                <Text style={styles.title_text}>Yêu cầu chụp hình</Text>
                            </View>
                            <View style={styles.box_btn}>
                                <Text style={styles.message_text}>Bạn có yêu cầu chụp hình báo cáo</Text>
                                {/* <Text style={styles.date_text}>Ngày yêu cầu: {this.state.dateRequest}</Text> */}
                                <TouchableOpacity
                                    onPress={this.onSubmit.bind(this)}
                                    style={styles.submit}
                                    underlayColor='transparent'>
                                    <Text style={styles.btn_text}>{Strings.confirm}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </Modal>
        );

    }
    onSubmit() {
        this.setState({ modalVisible: false });
        if (this.props.onResponse) {
            this.props.onResponse();
        }
    }
    openPopup(date) {
        this.setState({
            modalVisible: true,
            dateRequest: date
        })
    }
}
