import { StyleSheet } from 'react-native';
import Color from '../../global/colors'

module.exports = StyleSheet.create({
    modal_box: {
        backgroundColor: 'rgba(35, 35, 35, 0.5)',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%'
    },
    popup_box: {
        backgroundColor: 'white',
        borderRadius: 8,
        position: 'absolute',
        right: 24,
        left: 24,
        alignItems: 'center'
    },
    title_box: {
        height: 48,
        borderBottomWidth: 1,
        width: '100%',
        borderBottomColor: 'rgba(228, 228, 228, 0.5)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btn_text: {
        color: 'white',
        fontSize: 14,
        fontFamily: "Comfortaa-Regular"
    },
    btn_galary_text: {
        color: Color.dim_gray,
        fontSize: 14,
        fontFamily: "Comfortaa-Regular"
    },
    title_text: {
        color: Color.dim_gray,
        fontSize: 14,
        fontFamily: "Comfortaa-Bold"
    },
    message_text: {
        color: Color.dim_gray,
        fontSize: 12,
        fontFamily: "Comfortaa-Regular",
        marginTop: 24,
        alignSelf: 'center',
        textAlign: 'center'
    },
    date_text: {
        color: Color.dim_gray,
        fontSize: 10,
        fontFamily: "Comfortaa-Regular",
        marginTop: 8,
        alignSelf: 'center',
        textAlign: 'center'
    },
    submit: {
        marginTop: 22,
        marginBottom: 32,
        height: 40,
        width: '100%',
        // flex: 1,
        backgroundColor: Color.dark_blue,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    plate_input: {
        height: 40,
        // flex: 1,
        width: '100%',
        paddingLeft: 8,
        paddingRight: 8,
        fontSize: 14,
        marginTop: 32,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Color.platinum,
        color: Color.black,
        fontFamily: "Comfortaa-Bold",
        textAlign: 'center'
    },
    box_btn: {
        paddingLeft: 24,
        paddingRight: 24,
        width: '100%'
    },
    get_picture: {
        marginTop: 12,
        height: 40,
        marginBottom: 32,
        width: '100%',
        backgroundColor: Color.platinum,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    icon_cancel: {
        height: 48,
        width: 48,
        position: 'absolute',
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        height: 15,
        width: 15
    }
});
