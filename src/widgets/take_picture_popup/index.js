import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Modal,
    Image
} from 'react-native';
import Strings from '../../global/strings';
import ImagePicker from 'react-native-image-picker';
var options = {
    title: 'Chọn hình đại diện',
    maxWidth: 1280,
    maxHeight: 720,
    cancelButtonTitle: 'Thoát',
    takePhotoButtonTitle: 'Từ camera',
    chooseFromLibraryButtonTitle: 'Từ thư viện ảnh',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

var styles = require('./styles');
const PropTypes = require('prop-types');

export default class InputRow extends Component {
    static propTypes = {
        onResponse: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            imageIndex: -1
        }
    }

    render() {
        return (
            <Modal
                visible={this.state.modalVisible}
                onRequestClose={() => this.setState({ modalVisible: false })}
                transparent={true}>
                <TouchableWithoutFeedback onPress={() => this.setState({ modalVisible: false })} >
                    <View style={styles.modal_box}>
                        <TouchableWithoutFeedback>
                            <View style={styles.popup_box}>
                                <View style={styles.title_box}>
                                    <Text style={styles.title_text}>Chọn hình ảnh</Text>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ modalVisible: false })}
                                        style={styles.icon_cancel}
                                        underlayColor='transparent'>
                                        <Image style={styles.icon} resizeMode={'contain'} source={require('../../assets/images/icon_cancel.png')}></Image>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.box_btn}>
                                    <TouchableOpacity
                                        onPress={() => this.openCamera()}
                                        style={styles.submit}
                                        underlayColor='transparent'>
                                        <Text style={styles.btn_text}>{Strings.from_camera}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => this.openGalery()}
                                        style={styles.get_picture}
                                        underlayColor='transparent'>
                                        <Text style={styles.btn_galary_text}>{Strings.from_galary}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        );

    }
    openPopup(index) {
        this.setState({
            modalVisible: true,
            imageIndex: index
        })
    }
    openCamera() {
        this.setState({ modalVisible: false })
        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                if (this.props.onResponse) this.props.onResponse(this.state.imageIndex, source)
                // console.log(source);
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

            }
        });
    }
    openGalery() {
        this.setState({ modalVisible: false })
        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                if (this.props.onResponse) this.props.onResponse(this.state.imageIndex, source)
                // console.log(source);
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

            }
        });
    }
}
