import {
  Splash,
  Login,
  GetOtp,
  CreateNewPass,
  Main,
  Report,
  ReportList
} from './views'
import Const from '../global/const'

export default {
  splash: {
    screen: Splash,
    props: {}
  },
  login: {
    screen: Login,
    props: {}
  },
  get_otp: {
    screen: GetOtp,
    props: {}
  },
  create_new_pass: {
    screen: CreateNewPass,
    props: {}
  },
  main: {
    screen: Main,
    props: {}
  },
  report: {
    screen: Report,
    props: {}
  },
  report_list: {
    screen: ReportList,
    props: {}
  }
}