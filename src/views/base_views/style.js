import React, {
  StyleSheet
} from 'react-native';
import Color from '../../global/colors'

module.exports = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%'
  },
  popup_box: {
    position: 'absolute',
    right: 30,
    left: 30,
    backgroundColor: 'white',
    borderRadius: 5,
    alignItems: 'center',
    paddingLeft: 8,
    paddingRight: 8
  },
  popup_title: {
    fontSize: 16,
    color: Color.dark_blue,
    fontFamily: "Comfortaa-Bold",
    marginTop: 10
  },
  popup_message: {
    fontSize: 14,
    color: Color.dark,
    marginTop: 20,
    fontFamily: "Comfortaa-Regular",
  },
  popup_btn: {
    height: 35,
    width: 120,
    backgroundColor: Color.dark_blue,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20
  },
  popup_btn_text: {
    fontSize: 14,
    color: Color.smoke,
    fontFamily: "Comfortaa-Regular"
  }
});
