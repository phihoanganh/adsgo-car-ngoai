import {
    StyleSheet,
    Dimensions
} from 'react-native';
import Color from '../../global/colors'


module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '100%',
        height: '100%'
    },
    guide_pic_box: {
        height: 128,
        width: '100%',
        backgroundColor: Color.munsell
    },
    guide_pic_scroll: {
        paddingTop: 14,
        paddingBottom: 14
    },
    guide_item_box: {
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        width: (Dimensions.get('window').width - 42) / 2,
        height: 132,
        marginTop: 14,
        backgroundColor: Color.munsell,
        borderRadius: 5,
        borderColor: Color.platinum,
    },
    image_sample: {
        width: (Dimensions.get('window').width - 42) / 2,
        height: 160
    },
    image_sample_text: {
        fontSize: 10,
        color: Color.dim_gray,
        fontFamily: "Comfortaa-Regular",
    },
    sample_box: {
        height: 24,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    scroll_st: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingLeft: 14,
        paddingRight: 14,
        paddingBottom: 14,
        justifyContent: 'space-between'
    },
    take_pt_box: {
        width: (Dimensions.get('window').width - 42) / 2,
        // marginRight: 14,
        height: 132,
        marginTop: 14,
        backgroundColor: Color.munsell,
        borderRadius: 5,
        borderColor: Color.platinum,
        borderWidth: 1,
        alignItems: 'center'
    },
    take_pt_title_box: {
        height: 22,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Color.deep_saffron,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    take_pt_title_text: {
        fontSize: 12,
        color: 'white',
        fontFamily: "Comfortaa-Regular",
    },
    img_pt: {
        height: 90,
        marginTop: 10,
        // maxWidth: 147,
        width: (Dimensions.get('window').width - 42) / 2 - 12
    },
    upload_btn: {
        height: 28,
        width: (Dimensions.get('window').width - 42) / 2 - 28,
        borderRadius: 5,
        borderWidth: 1,
        marginTop: 6,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Color.ash_gray
    },
    upload_btn_text: {
        fontSize: 12,
        color: Color.dim_gray,
        fontFamily: "Comfortaa-Regular",
    },
    title_box: {
        height: 48,
        backgroundColor: Color.dark_blue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title_text: {
        fontSize: 14,
        color: 'white',
        fontFamily: "Comfortaa-Bold"
    },
    back_btn: {
        height: 48,
        width: 48,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon_back: {
        height: 14,
        width: 14
    },
    submit: {
        marginTop: 10,
        height: 40,
        flex: 1,
        marginRight: 24,
        marginLeft: 24,
        backgroundColor: Color.dark_blue,
        borderRadius: 5,
        borderColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn_text: {
        color: 'white',
        fontSize: 14,
        fontFamily: "Comfortaa-Regular",
        // fontWeight: 'bold'
    }
});