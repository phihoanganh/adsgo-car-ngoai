import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Image,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base_views'
import styles from './style'
import ImagePicker from 'react-native-image-picker';
import { sendReport } from '../../actions/view_action/reportAction';
import Const from '../../global/const'


var options = {
    title: 'Chọn hình đại diện',
    maxWidth: 1280,
    maxHeight: 720,
    cancelButtonTitle: 'Thoát',
    takePhotoButtonTitle: 'Từ camera',
    chooseFromLibraryButtonTitle: 'Từ thư viện ảnh',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

class Report extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            pictureBoxSize: 50,
            imgCuaVoLang: require('../../assets/images/icon_take_pt.png'),
            imgCuaDoiDien: require('../../assets/images/icon_take_pt.png'),
            imgDuoiXe: require('../../assets/images/icon_take_pt.png'),
            imgCongTo: require('../../assets/images/icon_take_pt.png'),
            upUriSources: [null, null, null, null]
        };

    }
    componentWillMount() {
        this.setState({
            pictureBoxSize: (Dimensions.get('window').width - 60) / 4
        });
    }
    renderBaseContent() {
        return (
            <View style={styles.container}>
                <View style={styles.title_box}>
                    <Text style={styles.title_text}>Chụp hình báo cáo</Text>
                    <TouchableOpacity
                        onPress={() => this.goBack()}
                        style={styles.back_btn}
                        underlayColor='transparent'>
                        <Image style={styles.icon_back} resizeMode={'contain'} source={require('../../assets/images/icon_back.png')}></Image>
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <View style={styles.scroll_st}>
                        <View style={styles.take_pt_box}>
                            <View style={styles.take_pt_title_box}>
                                <Text style={styles.take_pt_title_text}>Cửa cạnh vô lăng</Text>
                            </View>
                            <TouchableOpacity onPress={() => { this.takePicture(1) }}>
                                <Image style={styles.img_pt} resizeMode={'contain'} source={this.state.imgCuaVoLang}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.guide_item_box}>
                            <Image style={styles.image_sample} resizeMode={'contain'} source={require('../../assets/images/ads_trai.png')}></Image>
                        </View>
                        <View style={styles.take_pt_box}>
                            <View style={styles.take_pt_title_box}>
                                <Text style={styles.take_pt_title_text}>Cửa đối diện</Text>
                            </View>
                            <TouchableOpacity onPress={() => { this.takePicture(2) }}>
                                <Image style={styles.img_pt} resizeMode={'contain'} source={this.state.imgCuaDoiDien}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.guide_item_box}>
                            <Image style={styles.image_sample} resizeMode={'contain'} source={require('../../assets/images/ads_phai.png')}></Image>
                        </View>
                        <View style={styles.take_pt_box}>
                            <View style={styles.take_pt_title_box}>
                                <Text style={styles.take_pt_title_text}>Đuôi xe</Text>
                            </View>
                            <TouchableOpacity onPress={() => { this.takePicture(3) }}>
                                <Image style={styles.img_pt} resizeMode={'contain'} source={this.state.imgDuoiXe}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.guide_item_box}>
                            <Image style={styles.image_sample} resizeMode={'contain'} source={require('../../assets/images/ads_sau.png')}></Image>
                        </View>

                        <View style={styles.take_pt_box}>
                            <View style={styles.take_pt_title_box}>
                                <Text style={styles.take_pt_title_text}>Công tơ mét</Text>
                            </View>
                            <TouchableOpacity onPress={() => { this.takePicture(4) }}>
                                <Image style={styles.img_pt} resizeMode={'contain'} source={this.state.imgCongTo}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.guide_item_box}>
                            <Image style={styles.image_sample} resizeMode={'contain'} source={require('../../assets/images/ads_km.png')}></Image>
                        </View>
                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            this.sendReportImg()
                        }}
                        style={styles.submit}
                        underlayColor='transparent'>
                        <Text style={styles.btn_text}>Gửi hình</Text>
                    </TouchableOpacity>
                    <View style={{ height: 10 }} />
                </ScrollView>
            </View>
        );
    }
    onSendSuccess() {
        this.renderPopup(
            'Gửi thành công',
            'Chúng tôi sẽ kiểm tra ảnh trong thời gian sớm nhất',
            null,
            null,
            1,
            () => {
                this.goBack();
            }
        );
    }
    sendReportImg() {
        for (var i = 0; i < this.state.upUriSources.length; i++) {
            if (!this.state.upUriSources[i]) {
                this.renderPopup(
                    Const.REQUEST_ERROR_TITLE,
                    Const.NOT_ENOUGH_IMG_ERROR_MESSAGE,
                    require('../../assets/images/icon_fail.png'),
                    null,
                    2,
                    null
                );
                return;
            }
        }
        let licensePlate = this.props.loginPage.user_info.soXe;
        let requestId = this.props.requestReportData.request_list.id;
        let cmnd = this.props.loginPage.user_info.cmnd;

        this.props.sendReport(requestId, cmnd, licensePlate, this.state.upUriSources, this)

    }
    takePicture(index) {
        // Launch Camera:
        ImagePicker.launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                let d = new Date();
                let n = d.getTime();
                let data = new FormData();
                let upUriSourcesTemp = this.state.upUriSources;
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                switch (index) {
                    case 1:
                        data.append('email', this.props.loginPage.user_info.cmnd);
                        data.append('file', {
                            uri: source.uri,
                            type: 'image/jpeg', // or photo.type
                            name: '1_' + n + '' + this.props.loginPage.user_info.soXe
                        });
                        upUriSourcesTemp[0] = data;
                        this.setState({
                            imgCuaVoLang: source,
                            upUriSources: upUriSourcesTemp
                        });
                        break;
                    case 2:
                        data.append('email', this.props.loginPage.user_info.cmnd);
                        data.append('file', {
                            uri: source.uri,
                            type: 'image/jpeg', // or photo.type
                            name: '2_' + n + '' + this.props.loginPage.user_info.soXe
                        });
                        upUriSourcesTemp[1] = data;
                        this.setState({
                            imgCuaDoiDien: source,
                            upUriSources: upUriSourcesTemp
                        });

                        break;
                    case 3:
                        data.append('email', this.props.loginPage.user_info.cmnd);
                        data.append('file', {
                            uri: source.uri,
                            type: 'image/jpeg', // or photo.type
                            name: '3_' + n + '' + this.props.loginPage.user_info.soXe
                        });
                        upUriSourcesTemp[2] = data;
                        this.setState({
                            imgDuoiXe: source,
                            upUriSources: upUriSourcesTemp
                        });

                        break;
                    case 4:
                        data.append('email', this.props.loginPage.user_info.cmnd);
                        data.append('file', {
                            uri: source.uri,
                            type: 'image/jpeg', // or photo.type
                            name: '4_' + n + '' + this.props.loginPage.user_info.soXe
                        });
                        upUriSourcesTemp[3] = data;
                        this.setState({
                            imgCongTo: source,
                            upUriSources: upUriSourcesTemp
                        });
                        break;
                }
            }
        });
    }

    componentWillUnmount() {
        const { params } = this.props.navigation.state;
        if (params.actionBack) {
            params.actionBack()
        }
    }
}
function mapStateToProps(state) {
    return {
        // nav: state.nav
        loginPage: state.loginPage,
        requestReportData: state.requestReportData
    };
}

export default connect(mapStateToProps, { sendReport })(Report);