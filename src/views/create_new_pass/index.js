import React, { Component } from 'react';
import {
    View, Text, TextInput, TouchableOpacity, ScrollView, StatusBar, Image, Animated
} from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base_views'
import TextField from '../../widgets/text_field'
import Strings from '../../global/strings'
import styles from './style'

class GetOtp extends BaseView {
    constructor(props) {
        super(props);
        this.state = {

        };

    }
    renderBaseContent() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.logo_box}>
                    <Image style={styles.logo} resizeMode={'contain'} source={require('../../assets/images/logo.png')} ></Image>
                </View>
                <View style={styles.form}>
                    <TextField
                        label={Strings.new_pass}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        height={40}
                    />
                    <TextField
                        label={Strings.retype_pass}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        height={40}
                    />
                    <TextField
                        label={Strings.otp}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        height={40}
                    />
                    <TouchableOpacity
                        onPress={() => { this.resetPage(0, 'login', null) }}
                        style={styles.submit}
                        underlayColor='transparent'>
                        <Text style={styles.btn_text}>{Strings.confirm}</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => { }}>
                    <Text style={styles.forgot_pass}>{Strings.resend_otp}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { this.goBack() }}>
                    <Text style={styles.forgot_pass}>{Strings.retype_phone}</Text>
                </TouchableOpacity>
            </ScrollView>
        );
    }
}


export default connect(null, null)(GetOtp);