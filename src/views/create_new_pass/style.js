import React, {
    StyleSheet
} from 'react-native';

module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingBottom: 20
    },
    logo_box: {
        width: '100%',
        alignItems: 'center',
        marginTop: 90
    },
    logo: {
        width: 159,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center'
    },
    form: {
        marginTop: 40,
        paddingLeft: 32,
        paddingRight: 32,
        justifyContent: 'center',
        alignItems: 'center'
    },
    submit: {
        marginTop: 50,
        height: 50,
        width: 200,
        backgroundColor: '#02867C',
        borderRadius: 25,
        borderWidth: 1,
        borderColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn_text: {
        color: 'white',
        fontSize: 18,
        fontFamily: "Roboto-Regular",
        // fontWeight: 'bold'
    },
    forgot_pass: {
        fontSize: 14,
        color: '#676766',
        marginTop: 25,
        fontFamily: "Roboto-Italic",
        marginLeft: 32,
        marginRight: 32,
    }
});