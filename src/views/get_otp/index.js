import React, { Component } from 'react';
import {
    View, Text, TextInput, TouchableOpacity, ScrollView, StatusBar, Image, Animated
} from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base_views'
import TextField from '../../widgets/text_field'
import Strings from '../../global/strings'
import styles from './style'

class GetOtp extends BaseView {
    constructor(props) {
        super(props);
        this.state = {

        };

    }
    renderBaseContent() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.logo_box}>
                    <Image style={styles.logo} resizeMode={'contain'} source={require('../../assets/images/logo.png')} ></Image>
                </View>
                <View style={styles.form}>
                    <TextField
                        label={Strings.username_hint}
                        highlightColor={'#02867C'}
                        textFocusColor={'#676766'}
                        textBlurColor={'#A5A8AA'}
                        height={40}
                    />

                    <TouchableOpacity
                        onPress={()=>{this.pushPage('create_new_pass', null)}}
                        style={styles.submit}
                        underlayColor='transparent'>
                        <Text style={styles.btn_text}>{Strings.confirm}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => { this.goBack() }}>
                        <Text style={styles.forgot_pass}>{Strings.go_back_login}</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}


export default connect(null, null)(GetOtp);