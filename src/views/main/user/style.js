import React, {
    StyleSheet
} from 'react-native';
import Color from '../../../global/colors'

module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white'
    },
    modal_box: {
        backgroundColor: 'rgba(35, 35, 35, 0.5)',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%'
    },
    popup_box: {
        backgroundColor: 'white',
        borderRadius: 5,
        position: 'absolute',
        right: 30,
        left: 30,
        borderWidth: 1,
        alignItems: 'center',
        borderColor: Color.border,
    },
    submit: {
        height: 40,
        width: '100%',
        backgroundColor: Color.dark_blue,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 24
    },
    btn_text: {
        color: 'white',
        fontSize: 14,
        fontFamily: "Comfortaa-Regular",
        // fontWeight: 'bold'
    },
    title_text: {
        color: Color.dim_gray,
        fontSize: 14,
        fontFamily: "Comfortaa-Regular",
        // fontWeight: 'bold'
    },
    close_box: {
        height: 48,
        width: 48,
        right: 0,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center'
    },
    form_box: {
        paddingLeft: 24,
        paddingRight: 24,
        width: '100%'
    },
    title_box: {
        height: 48,
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: Color.platinum,
        justifyContent: 'center',
        alignItems: 'center'
    },
    name_box: {
        height: 166,
        width: '100%',
        backgroundColor: Color.dark_blue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon_user: {
        height: 58,
        width: 58
    },
    user_name_content: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    name_text: {
        color: 'white',
        fontSize: 14,
        marginTop: 8,
        fontFamily: "Comfortaa-Regular"
    },
    driver_info_box: {
        height: 48,
        width: '100%',
        backgroundColor: Color.munsell,
        flexDirection: 'row'
    },
    diver: {
        height: 24,
        width: 2,
        backgroundColor: 'white',
        marginTop: 12
    },
    info_box: {
        flex: 1,
        height: '100%',
        justifyContent: 'center'
    },
    info_box_right: {
        flex: 1,
        height: '100%',
        justifyContent: 'center'
    },
    info_text: {
        color: Color.dim_gray,
        fontSize: 14,
        fontFamily: "Comfortaa-Regular",
        marginLeft: 24
    },
    info_text_right: {
        color: Color.dim_gray,
        fontSize: 14,
        fontFamily: "Comfortaa-Regular",
        marginRight: 24,
        alignSelf: 'flex-end'
    },
    total_km_title_text: {
        color: 'white',
        fontSize: 18,
        fontFamily: "Comfortaa-Regular"
    },
    total_km_box: {
        flex: 1,
        marginLeft: 24,
        marginRight: 24,
        marginTop: 24,
        height: 140,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: Color.munsell
    },
    take_pc_his_btn: {
        flex: 1,
        marginLeft: 24,
        marginRight: 24,
        marginTop: 24,
        height: 48,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: Color.ash_gray,
        justifyContent: 'center',
        alignItems: 'center'
    },
    action_box: {
        paddingLeft: 24,
        paddingRight: 24,
        height: 100,
        marginTop: 24,
        flexDirection: 'row'
    },
    change_pass_box: {
        height: 100,
        flex: 1,
        backgroundColor: Color.munsell,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    total_km_title_box: {
        height: 48,
        backgroundColor: Color.deep_saffron,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    total_km: {
        width: '100%',
        height: 92,
        justifyContent: 'center',
        alignItems: 'center',
    },
    km_contain: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    km_number: {
        fontSize: 24,
        color: Color.dim_gray,
        fontFamily: "Comfortaa-Regular"
    },
    km_km: {
        fontSize: 14,
        color: Color.dim_gray,
        fontFamily: "Comfortaa-Bold",
        marginBottom: 3
    },
    take_pc_his_btn_text: {
        fontSize: 14,
        color: Color.dim_gray,
        fontFamily: "Comfortaa-Regular",
    },
    action_text: {
        fontSize: 12,
        color: Color.dim_gray,
        fontFamily: "Comfortaa-Regular",
        marginTop: 8
    },
    icon_pass: {
        height: 28,
        width: 28
    },
    icon_logout: {
        height: 28,
        width: 24
    },
    icon_close: {
        width: 16,
        height: 16
    }
});