import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Modal,
    TouchableWithoutFeedback,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base_view'
import styles from './style'
import TextField from '../../../widgets/text_field'
import Strings from '../../../global/strings'
import { getUserInfo, changePass, logout } from '../../../actions/view_action/userAction';

class User extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            changePassBtnDisable: false
        }
    }
    renderBaseContent() {
        let userInfo = this.props.loginPage.user_info;
        return (
            <ScrollView style={styles.container}>
                <View style={styles.name_box}>
                    <View style={styles.user_name_content}>
                        <Image style={styles.icon_user} resizeMode={'contain'} source={require('../../../assets/images/icon_avatar.png')}></Image>
                        <Text style={styles.name_text}>{userInfo.tenTaiXe}</Text>
                    </View>
                </View>
                <View style={styles.driver_info_box}>
                    <View style={styles.info_box}>
                        <Text style={styles.info_text}>{userInfo.sdt}</Text>
                    </View>
                    <View style={styles.diver}></View>
                    <View style={styles.info_box_right}>
                        <Text style={styles.info_text_right}>{userInfo.soXe}</Text>
                    </View>
                </View>
                <View style={styles.total_km_box}>
                    <View style={styles.total_km_title_box}>
                        <Text style={styles.total_km_title_text}>Tổng số tất cả km đã chạy</Text>
                    </View>
                    <View style={styles.total_km}>
                        <View style={styles.km_contain}>
                            <Text style={styles.km_number}>{userInfo.tongKM}</Text>
                            <Text style={styles.km_km}>km</Text>
                        </View>
                    </View>
                </View>
                <TouchableOpacity style={styles.take_pc_his_btn} onPress={() => { this.pushRootPage('report_list', null) }}>
                    <Text style={styles.take_pc_his_btn_text}>Lịch sử chụp hình báo cáo</Text>
                </TouchableOpacity>
                <View style={styles.action_box}>
                    <TouchableOpacity style={[styles.change_pass_box, { marginRight: 24 }]} onPress={() => { this.setState({ modalVisible: true }) }}>
                        <Image style={styles.icon_pass} resizeMode={'contain'} source={require('../../../assets/images/icon_resetpass.png')}></Image>
                        <Text style={styles.action_text}>Đổi mật khẩu</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.change_pass_box} onPress={() => {
                        this.props.logout(this.props.loginPage.user_info.cmnd, this);
                    }}>
                        <Image style={styles.icon_logout} resizeMode={'contain'} source={require('../../../assets/images/icon_logout.png')}></Image>
                        <Text style={styles.action_text}>Đăng xuất</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    visible={this.state.modalVisible}
                    // animationType={'slide'}
                    onRequestClose={() => this.setState({ modalVisible: false })}
                    transparent={true}>

                    <TouchableWithoutFeedback onPress={() => this.setState({ modalVisible: false })} >
                        <View style={styles.modal_box}>
                            <TouchableWithoutFeedback>
                                <View style={styles.popup_box}>
                                    <View style={styles.title_box}>
                                        <Text style={styles.title_text}>{Strings.change_pass}</Text>
                                        <TouchableOpacity style={styles.close_box} onPress={() => this.setState({ modalVisible: false })}>
                                            <Image style={styles.icon_close} resizeMode={'contain'} source={require('../../../assets/images/icon_cancel.png')}></Image>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.form_box}>
                                        <TextField
                                            ref="tf_old_password"
                                            label={Strings.old_pass}
                                            secureText={true} />
                                        <TextField
                                            ref="tf_new_password"
                                            label={Strings.new_pass}
                                            secureText={true} />
                                        <TextField
                                            ref="tf_retype_password"
                                            label={Strings.retype_pass}
                                            secureText={true} />
                                        <TouchableOpacity
                                            onPress={this.changePass.bind(this)}
                                            style={[styles.submit, this.state.changePassBtnDisable && { backgroundColor: '#f2f2f2' }]}
                                            underlayColor='transparent'
                                            disabled={this.state.changePassBtnDisable}>
                                            <Text style={styles.btn_text}>{Strings.agree}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </ScrollView >
        );
    }
    changePass() {
        let oldPass = this.refs.tf_old_password.getValue();
        let newPass = this.refs.tf_new_password.getValue();
        let retypePass = this.refs.tf_retype_password.getValue();
        console.log(oldPass);
        console.log(newPass);
        console.log(retypePass);

        if (oldPass == '' || oldPass == undefined || oldPass == null) {
            console.log('null');
            this.renderPopup(
                'Thông báo',
                'Vui lòng nhập mật khẩu cũ',
                require('../../../assets/images/icon_fail.png'),
                null,
                2,
                null
            );
            return;
        }
        if (newPass == '' || newPass == undefined || newPass == null) {
            this.renderPopup(
                'Thông báo',
                'Vui lòng nhập mật khẩu mới',
                require('../../../assets/images/icon_fail.png'),
                null,
                2,
                null
            );
            return;
        }
        if (newPass !== retypePass) {
            this.renderPopup(
                'Thông báo',
                'Mật khẩu nhập lại không trùng khớp',
                require('../../../assets/images/icon_fail.png'),
                null,
                2,
                null
            );
            return;
        }
        this.props.changePass(this.props.loginPage.user_info.cmnd, oldPass, newPass, this);
    }
    changingPass(value) {
        this.setState({
            changePassBtnDisable: value
        })
    }
    componentDidMount() {
        this.props.getUserInfo(this.props.loginPage.user_info.cmnd, this);
        // 
        // setLoading(true);
    }
}
function mapStateToProps(state) {
    return {
        loginPage: state.loginPage
    };
}
export default connect(mapStateToProps, { getUserInfo, changePass, logout })(User);