import {
    Home,
    Support,
    CampaignHis,
    User
} from './views'

export default {
    home: {
        screen: Home,
        props: {}
    },
    support: {
        screen: Support,
        props: {}
    },
    campaign_his: {
        screen: CampaignHis,
        props: {}
    },
    user: {
        screen: User,
        props: {}
    }
}