import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import Color from '../../../global/colors';
module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '100%',
        height: '100%'
    },
    img_header: {
        width: '100%',
        height: Dimensions.get('window').width * 427 / 720
    },
    total_km_box: {
        flex: 1,
        marginTop: -45,
        borderRadius: 5,
        // height: 260,
        marginLeft: 14,
        marginRight: 14,
        backgroundColor: Color.deep_saffron,
        alignItems: 'center',
        paddingLeft: 24,
        paddingRight: 24
    },
    action_box: {
        marginTop: 24,
        paddingLeft: 14,
        paddingRight: 14,
        marginBottom: 14,
        flexDirection: 'row'
    },
    take_picture_box: {
        flex: 1,
        margin: 24,
        borderRadius: 5,
        height: 110,
        marginLeft: 14,
        marginRight: 14,
        backgroundColor: Color.dim_gray,
    },
    km_number: {
        fontSize: 36,
        color: 'white',
        fontFamily: "Comfortaa-Regular"
    },
    km_km: {
        fontSize: 18,
        color: 'white',
        fontFamily: "Comfortaa-Bold",
        marginBottom: 5
    },
    icon_car: {
        width: 82,
        height: 26,
        marginTop: 39,
        marginBottom: 39
    },
    icon_photo: {
        width: 44,
        height: 35,
    },
    icon_checkin: {
        height: 110,        // marginTop: 16,
        width: 50,
        borderBottomLeftRadius: 5,
        borderTopLeftRadius: 5
    },
    total_km: {
        width: '100%',
        height: 60,
        marginLeft: 24,
        marginRight: 24,
        marginBottom: 24,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    km_contain: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    text_title: {
        fontSize: 22,
        color: 'white',
        marginTop: 25,
        fontFamily: "Comfortaa-Regular",
    },
    take_picture_text: {
        flex: 1,
        fontSize: 24,
        color: 'white',
        fontFamily: "Comfortaa-Regular",
        alignSelf: 'center',
        textAlign: 'center',
        marginTop: 32,
        position: 'absolute'
    }
});