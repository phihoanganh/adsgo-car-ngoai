import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base_view'
import styles from './style'
// import CheckInPopup from '../../../widgets/checkin_popup';
import RequestPopup from '../../../widgets/confirm_popup'
import { getImagesRequest } from '../../../actions/view_action/homeAction';
import { getUserInfo } from '../../../actions/view_action/userAction';


// import ss from '../../'

class Home extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    renderBaseContent() {
        let userInfo = this.props.loginPage.user_info;
        return (
            <ScrollView style={styles.container}>
                <Image style={styles.img_header} resizeMode={'contain'} source={require('../../../assets/images/img_header.png')}></Image>
                <View style={styles.total_km_box}>
                    <Text style={styles.text_title}>Tổng số km</Text>
                    <Text style={[styles.text_title, { marginTop: 0 }]}>đã chạy cho chiến dịch</Text>
                    <Image style={styles.icon_car} resizeMode={'contain'} source={require('../../../assets/images/car.png')}></Image>
                    <View style={styles.total_km}>
                        <View style={styles.km_contain}>
                            <Text style={styles.km_number}>{userInfo.tongKMCD}</Text>
                            <Text style={styles.km_km}>km</Text>
                        </View>
                    </View>
                </View>
                {/* <View style={styles.action_box}> */}
                {/* <TouchableOpacity style={[styles.take_picture_box, { marginRight: 14 }]} onPress={() => { this.pushRootPage('report_list', null) }}>
                        <Image style={styles.icon_photo} resizeMode={'contain'} source={require('../../../assets/images/photo.png')}></Image>
                        <Text style={styles.take_picture_text}>CHỤP HÌNH BÁO CÁO</Text>
                    </TouchableOpacity> */}
                <View style={styles.take_picture_box}>
                    <Image style={styles.icon_checkin} resizeMode={'cover'} source={require('../../../assets/images/icon_checkin.png')}></Image>
                    <Text style={styles.take_picture_text}>Hoàn thành: 40%</Text>
                </View>
                {/* </View> */}
                {/* <CheckInPopup
                    ref='checkin_popup' /> */}
                <RequestPopup
                    ref='request_popup_ref'
                    onResponse={() => {
                        this.setLoading(true);
                        this.pushRootPage('report', { actionBack: this.checkRequestImage.bind(this) });
                    }} />
            </ScrollView >
        );
    }
    openReportPage() {
    }
    checkRequestImage() {
        this.props.getImagesRequest(this);
    }
    openRequestImagePopup() {
        this.refs.request_popup_ref.openPopup();
    }
    componentDidMount() {
        this.checkRequestImage();
        this.props.getUserInfo(this.props.loginPage.user_info.cmnd, this);
    }
}
function mapStateToProps(state) {
    return {
        loginPage: state.loginPage
    };
}
export default connect(mapStateToProps, { getImagesRequest, getUserInfo })(Home);