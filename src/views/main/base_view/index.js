import React, { Component } from 'react';
import {
    View,
    ActivityIndicator
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import styles from './style';
import Popup from '../../../widgets/popup'

class BaseView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            containerMarginTop: 0,
            isPopupOpen: false,
            popupView: null
        }
    }
    render() {
        return (
            <View style={styles.container}>
                {this.renderBaseContent()}
                {this.state.isPopupOpen &&
                    this.state.popupView
                }
            </View>
        );
    }
    renderPopup(title, message, image, closeText, btnStyle, afterCloseFunction) {
        var mView = (
            <Popup
                afterCloseFunction={() => {
                    this.disablePopup();
                    if (afterCloseFunction) {
                        afterCloseFunction();
                    }
                }}
                disablePopup={this.disablePopup.bind(this)}
                image={image}
                title={title}
                message={message}
                closeText={closeText}
                isOpen={true}
                btnStyle={btnStyle} />
        );
        this.setState({
            popupView: mView,
            isPopupOpen: true
        });
    }
    disablePopup() {
        this.setState({
            popupView: null,
            isPopupOpen: false
        });
    }
    pushPage(pageView, params) {
        const { navigate } = this.props.navigation;
        navigate(pageView, params);
    }
    resetPage(index, pageView, params) {
        const { dispatch } = this.props.navigation;
        dispatch(NavigationActions.reset({
            index: index,
            key: null,
            actions: [NavigationActions.navigate({ routeName: pageView, params: params })]
        }))
    }
    pushRootPage(pageView, params) {
        let { navigate } = this.props.screenProps.rootNavigation;
        navigate(pageView, params);
    }
    resetRootPage(index, pageView, params) {
        let { dispatch } = this.props.screenProps.rootNavigation;
        dispatch(NavigationActions.reset({
            index: index,
            key: null,
            actions: [NavigationActions.navigate({ routeName: pageView, params: params })]
        }))
    }
    goBack() {
        const { goBack } = this.props.navigation;
        goBack();
    }
    setLoading(value) {
        let { setLoading } = this.props.screenProps;
        setLoading(value);
    }
}

module.exports = BaseView;