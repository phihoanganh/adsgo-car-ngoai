import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    PermissionsAndroid,
    StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { addNavigationHelpers, NavigationActions } from 'react-navigation';
import { StackNavigator } from 'react-navigation';
import BaseView from '../base_views'
import routes from './route'
import Const from '../../global/const';
import Color from '../../global/colors';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import {
    Connection,
    Queue,
    Exchange
} from 'react-native-rabbitmq';

let connection;
let queue;
let exchange;
let i = 1;
var isconnect = false;
const config = {
    host: '54.255.195.77',
    port: 8080,
    username: 'adsgo',
    password: '123@qazwsx',
    virtualhost: '/'
}
const gpsSetting = {
    enableHighAccuracy: true,
    timeout: 2000,
    maximumAge: 0,
    distanceFilter: 0.01
}

const BasicApp = StackNavigator(routes, {
    initialRouteName: 'home',
    headerMode: 'none',
    navigationOptions: {
        gesturesEnabled: false
    }
});

class Main extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            curentRoute: 'home'
        }
    }
    //goBack()
    renderBaseContent() {
        return (
            <View style={styles.container}>
                <BasicApp
                    ref={(mRef) => this.childNavigation = mRef}
                    screenProps={{ rootNavigation: this.props.navigation, getchildNav: this.getchildNav.bind(this), setLoading: this.setLoading.bind(this) }} />
                <View style={styles.tabbar}>
                    {this.renderTabarItems()}
                </View>
            </View >
        );
    }
    renderTabarItems() {
        let tabbarData = Const.tab_menu;
        var views = [];
        for (var i = 0; i < tabbarData.length; i++) {
            views.push(
                this.renderTabarItem(tabbarData[i], i)
            );
        }
        return views;
    }
    renderTabarItem(item, index) {
        return (
            < TouchableOpacity key={'tab' + index} style={styles.tabbar_btn} onPress={() => { this.changePage(item.route, null) }}>
                <Image style={[styles.tab_img, (this.state.curentRoute == item.route) ? { tintColor: Color.dark_blue } : { tintColor: Color.dim_gray }]} resizeMode={'contain'} source={item.img} ></Image>
                <Text style={[styles.tabbar_text, (this.state.curentRoute == item.route) ? { color: Color.dark_blue } : { color: Color.dim_gray }]}>{item.name}</Text>
            </TouchableOpacity >);
    }
    changePage(pageView, params) {
        let { dispatch } = this.childNavigation._navigation;
        this.setState({ curentRoute: pageView });
        // navigate('Setup');
        dispatch(NavigationActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: pageView, params: params })]
        }))
    }
    getchildNav() {
        if (this.childNavigation && this.childNavigation.state) {
            return this.childNavigation.state.nav;
        }
        return { index: 0, routes: [] };
    }
    async requestGPSPermission(context) {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Yêu cầu GPS',
                    'message': 'Vui lòng bật GPS để tính đoạn đường di chuyển.'
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED || granted == true) {
                console.log("Đã nhận")
                let GPSid = navigator.geolocation.watchPosition(
                    function (data) {
                        var geoDt = data.coords;
                        if (!geoDt) geoDt = {};
                        let licensePlate = context.props.loginPage.user_info.soXe;
                        geoDt.licenseplace = licensePlate;
                        if (!isconnect)
                            context.connectRabbitmq(geoDt);
                        console.log(geoDt);
                        if (isconnect) {
                            let routing_key = 'adsgo-gps';
                            let properties = {
                                expiration: 10000
                            }
                            exchange.publish(JSON.stringify(geoDt), routing_key, properties)
                        }
                    }, function (error) {
                        console.log(error);

                        LocationServicesDialogBox.checkLocationServicesIsEnabled({
                            message: "<h2 style='color: #0af13e'>GPS đang tắt</h2>Bạn cần bật GPS để hệ thống đo lường di chuyển của xe",
                            ok: "Bật GPS",
                            cancel: "Bỏ qua",
                            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
                            showDialog: true, // false => Opens the Location access page directly
                            openLocationServices: true, // false => Directly catch method is called if location services are turned off
                            preventOutSideTouch: true, // true => To prevent the location services window from closing when it is clicked outside
                            preventBackClick: true, // true => To prevent the location services popup from closing when it is clicked back button
                            providerListener: false // true ==> Trigger locationProviderStatusChange listener when the location state changes
                        }).then(function (success) {
                            // context.requestGPSPermission(context);
                            console.log("Bat lai gps")
                            navigator.geolocation.clearWatch(GPSid);
                            navigator.geolocation.watchPosition(
                                function (data) {
                                    var geoDt = data.coords;
                                    if (!geoDt) geoDt = {};
                                    let licensePlate = context.props.loginPage.user_info.soXe;
                                    geoDt.licenseplace = licensePlate;
                                    if (!isconnect)
                                        context.connectRabbitmq(geoDt);
                                    console.log(geoDt);
                                    if (isconnect) {
                                        let routing_key = 'adsgo-gps';
                                        let properties = {
                                            expiration: 10000
                                        }
                                        exchange.publish(JSON.stringify(geoDt), routing_key, properties)
                                    }
                                }, function (error) {

                                }, gpsSetting);
                        }).catch((error) => {
                            console.log(error.message); // error.message => "disabled"
                        });
                    }, gpsSetting);
            } else {
                console.log("permission denied")
            }
        } catch (err) {
            console.warn(err)
        }
    }

    connectRabbitmq(geoDt) {
        console.log('connectRabbitmq');
        connection = new Connection(config)
        connection.connect();
        connection.on('error', (event) => {
            console.log('connection error');
            console.log(event);
        });

        connection.on('connected', (event) => {
            console.log('connected');
            isconnect = true;
            this.setState({
                status: 'connected'
            });
            // queue = new Queue(connection, {
            //     name: 'adsgo-gps1',
            //     passive: false,
            //     durable: false,
            //     exclusive: false,
            //     consumer_arguments: { 'x-priority': 1 }
            // });

            exchange = new Exchange(connection, {
                name: 'adsgo-gps',
                type: 'fanout',
                durable: true,
                autoDelete: false,
                internal: false
            });

            //send first message
            let routing_key = 'adsgo-gps';
            let properties = {
                expiration: 10000
            }
            exchange.publish(JSON.stringify(geoDt), routing_key, properties)


            // queue.bind(exchange, 'adsgo-gps');

            // // Receive one message when it arrives
            // queue.on('message', (data) => {
            //     console.log('message');
            //     console.log(data);
            // });

            // // Receive all messages send with in a second
            // queue.on('messages', (data) => {
            //     // console.log('messages from ' + dvID);
            //     console.log(data);
            // });
        });
    }

    componentDidMount() {
        //Nho ios và android
        this.requestGPSPermission(this);

    }
    componentWillUnmount() {
        if (isconnect) {
            connection.close();
        }
    }
}
function mapStateToProps(state) {
    return {
        loginPage: state.loginPage
    };
}
export default connect(mapStateToProps, null)(Main);
const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%"
    },
    tabbar_text: {
        fontSize: 10,
        fontFamily: "Comfortaa-Bold",
    },
    tabbar: {
        flexDirection: 'row',
        height: 60,
        width: '100%',
        backgroundColor: Color.munsell
    },
    tabbar_btn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tab_img: {
        width: 25,
        height: 25,
        marginTop: 7,
        marginBottom: 5,
        justifyContent: 'center',
        alignItems: 'center'
    }
});