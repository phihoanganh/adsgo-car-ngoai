import React, {
    StyleSheet
} from 'react-native';
import Color from '../../../global/colors'

module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '100%',
        height: '100%'
    },
    row: {
        borderBottomColor: Color.platinum,
        borderBottomWidth: 1,
        marginRight: 24,
        marginLeft: 24,
        paddingTop: 14,
        paddingBottom: 14
    },
    title_box: {
        height: 48,
        backgroundColor: Color.dark_blue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title_text: {
        fontSize: 14,
        color: 'white',
        fontFamily: "Comfortaa-Bold"
    },
    text_name: {
        fontSize: 14,
        color: Color.black,
        fontFamily: "Comfortaa-Bold",
        backgroundColor: 'transparent'
    },
    flat_list_mess: {
        fontSize: 14,
        color: Color.black,
        fontFamily: "Comfortaa-Bold",
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    text_info: {
        fontSize: 12,
        color: Color.black,
        fontFamily: "Comfortaa-Regular",
        backgroundColor: 'transparent',
        marginTop: 5
    },
    // scrollview: {
    //     marginBottom: 40,
    //     // paddingBottom: 100
    // }
});