import React, { Component } from 'react';
import {
    View, Text, ScrollView,
    FlatList,
    TouchableOpacity,
    RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base_view'
import styles from './style'
import Strings from '../../../global/strings'
import { getCampaignList } from '../../../actions/view_action/campaignAction';


class CampaignHis extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    renderBaseContent() {
        let campaignList = this.props.campaignPage.campaign_list
        let getError = this.props.campaignPage.get_campaign_error
        let loadDone = this.props.campaignPage.loadDone
        console.log(getError)
        return (
            <View style={styles.container}>
                <View style={styles.title_box}>
                    <Text style={styles.title_text}>{Strings.list_campain_his}</Text>
                </View>
                {getError ?
                    <Text style={styles.flat_list_mess}>Không tải được danh sách</Text> :
                    <FlatList
                        ListHeaderComponent={() => ((campaignList.length == 0 && loadDone) &&
                            <Text style={styles.flat_list_mess}>Không có chiến dịch nào</Text>)
                        }
                        data={campaignList}
                        renderItem={({ item, index }) => this.renderItems(item, index)}
                        keyExtractor={(item, index) => index}
                    />
                }
            </View>
        );
    }
    onRefresh() {

    }
    renderItems(item, index) {
        return (
            <View style={styles.row}>
                <Text style={styles.text_name}>{item.name}</Text>
                <Text style={styles.text_info}>Thời gian: {item.thoiGianBatDau + " - " + item.thoiGianKetThuc}</Text>
                <Text style={styles.text_info}>Số km đã chạy: {item.tongKmDaChay
                }</Text>
            </View>
        );
    }
    componentDidMount() {
        console.log(this.props.loginPage);
        this.props.getCampaignList(this.props.loginPage.user_info.cmnd, this)
    }
}
function mapStateToProps(state) {
    return {
        campaignPage: state.campaignPage,
        loginPage: state.loginPage
    };
}
export default connect(mapStateToProps, { getCampaignList })(CampaignHis);
