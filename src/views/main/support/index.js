import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Image,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base_view';
import styles from './style';
import Strings from '../../../global/strings';
import Color from '../../../global/colors';
import PicturePopup from '../../../widgets/take_picture_popup';
import { sendRequestSp } from '../../../actions/view_action/supportAction';
import Const from '../../../global/const';

class Support extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            imgSource1: require('../../../assets/images/add_image.png'),
            imgSource2: require('../../../assets/images/add_image.png'),
            imgSource3: require('../../../assets/images/add_image.png'),
            imgSource4: require('../../../assets/images/add_image.png'),
            textCount: '0/250',
            upUriSources: [null, null, null, null],
            message: ''
        }
    }
    componentWillMount() {
        this.setState({
            pictureBoxSize: (Dimensions.get('window').width - 120) / 4
        });
    }
    renderBaseContent() {
        return (
            <View style={styles.container}>
                <View style={styles.title_box}>
                    <Text style={styles.title_text}>Hỗ trợ xe</Text>
                </View>
                <ScrollView style={styles.scrollview}>
                    <Text style={styles.note_text}>Vui lòng nhập thông tin và hình ảnh cần hỗ trợ</Text>
                    <TextInput
                        placeholder="Nhập thông tin"
                        style={styles.text_input}
                        maxLength={250}
                        value={this.state.message}
                        onChangeText={(text) => {
                            this.setState({
                                textCount: text.length + "/250",
                                message: text
                            });
                        }}
                        placeholderTextColor={Color.ash_gray}
                        underlineColorAndroid={'transparent'}
                        multiline={true} />
                    <Text style={styles.count_text}>{this.state.textCount}</Text>
                    <Text style={styles.attach_title_text}>Đính kèm ảnh</Text>
                    <View style={styles.attach_box}>
                        <PicturePopup
                            ref='popup_picture'
                            onResponse={this.onImagesResponse.bind(this)}></PicturePopup>
                        <TouchableOpacity onPress={() => this.refs.popup_picture.openPopup(1)}>
                            <Image style={{ width: this.state.pictureBoxSize, height: this.state.pictureBoxSize }} resizeMode={'cover'} source={this.state.imgSource1}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.refs.popup_picture.openPopup(2)}>
                            <Image style={{ width: this.state.pictureBoxSize, height: this.state.pictureBoxSize }} resizeMode={'cover'} source={this.state.imgSource2}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.refs.popup_picture.openPopup(3)}>
                            <Image style={{ width: this.state.pictureBoxSize, height: this.state.pictureBoxSize }} resizeMode={'cover'} source={this.state.imgSource3}></Image>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.refs.popup_picture.openPopup(4)}>
                            <Image style={{ width: this.state.pictureBoxSize, height: this.state.pictureBoxSize }} resizeMode={'cover'} source={this.state.imgSource4}></Image>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            this.onSendRequest();
                        }}
                        style={styles.submit}
                        underlayColor='transparent'>
                        <Text style={styles.btn_text}>{Strings.send_support}</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    }
    onSendSuccess() {
        this.renderPopup(
            'Gửi yêu cầu thành công',
            'Chúng tôi sẽ phản hồi trong thời gian sớm nhất',
            null,
            null,
            1,
            null
        );
        this.setState({
            imgSource1: require('../../../assets/images/add_image.png'),
            imgSource2: require('../../../assets/images/add_image.png'),
            imgSource3: require('../../../assets/images/add_image.png'),
            imgSource4: require('../../../assets/images/add_image.png'),
            textCount: '0/250',
            message: ''
        });
    }
    onSendRequest() {
        // console.log(this.state.upUriSources)
        if (this.state.message.length < 20) {
            this.renderPopup(
                Const.NOTIFY_TITLE,
                Const.MAX_LENGTH_20_ERROR,
                require('../../../assets/images/icon_fail.png'),
                null,
                2,
                null
            );
            return;
        }
        this.props.sendRequestSp(this.props.loginPage.user_info.cmnd, this.state.upUriSources, this.state.message, this)
    }
    onImagesResponse(index, source) {
        let d = new Date();
        let n = d.getTime();
        let data = new FormData();
        let upUriSourcesTemp = this.state.upUriSources;
        switch (index) {
            case 1:
                data.append('email', this.props.loginPage.user_info.cmnd);
                data.append('file', {
                    uri: source.uri,
                    type: 'image/jpeg', // or photo.type
                    name: '1_' + n + '' + this.props.loginPage.user_info.soXe
                });
                upUriSourcesTemp[0] = data;
                this.setState({
                    imgSource1: source,
                    upUriSources: upUriSourcesTemp
                });
                break;
            case 2:
                data.append('email', this.props.loginPage.user_info.cmnd);
                data.append('file', {
                    uri: source.uri,
                    type: 'image/jpeg', // or photo.type
                    name: '2_' + n + '' + this.props.loginPage.user_info.soXe
                });
                upUriSourcesTemp[1] = data;
                this.setState({
                    imgSource2: source,
                    upUriSources: upUriSourcesTemp
                });
                break;
            case 3:
                data.append('email', this.props.loginPage.user_info.cmnd);
                data.append('file', {
                    uri: source.uri,
                    type: 'image/jpeg', // or photo.type
                    name: '3_' + n + '' + this.props.loginPage.user_info.soXe
                });
                upUriSourcesTemp[2] = data;
                this.setState({
                    imgSource3: source,
                    upUriSources: upUriSourcesTemp
                });
                break;
            case 4:
                data.append('email', this.props.loginPage.user_info.cmnd);
                data.append('file', {
                    uri: source.uri,
                    type: 'image/jpeg', // or photo.type
                    name: '4_' + n + '' + this.props.loginPage.user_info.soXe
                });
                upUriSourcesTemp[3] = data;
                this.setState({
                    imgSource4: source,
                    upUriSources: upUriSourcesTemp
                });
                break;
        }
    }
    componentDidMount() {

    }
}
function mapStateToProps(state) {
    return {
        // nav: state.nav
        loginPage: state.loginPage
    };
}
export default connect(mapStateToProps, { sendRequestSp })(Support);