import React, {
    StyleSheet
} from 'react-native';
import Color from '../../../global/colors';

module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: '100%'
    },
    scrollview: {
        paddingLeft: 24,
        paddingRight: 24,
        marginTop: 24
    },
    title_text: {
        fontSize: 14,
        color: 'white',
        fontFamily: "Comfortaa-Bold"
    },
    text_input: {
        fontSize: 12,
        color: Color.black,
        fontFamily: "Comfortaa-Regular",
        minHeight: 190,
        borderWidth: 1,
        borderColor: Color.munsell,
        borderRadius: 2,
        marginTop: 26,
        padding: 12,
        paddingTop: 15,
        textAlignVertical: 'top'
    },
    underline: {
        backgroundColor: 'black',
        width: '100%',
        height: 0.5
    },
    attach_text: {
        fontSize: 18,
        color: 'gray',
        fontFamily: "Comfortaa-Regular"
    },
    count_text: {
        fontSize: 9,
        color: Color.ash_gray,
        fontFamily: "Comfortaa-Regular",
        alignSelf: 'flex-end',
        marginTop: 8
    },
    attach_box: {
        marginTop: 24,
        width: '100%',
        // paddingLeft: 24,
        // paddingRight: 24,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    attach_title_text: {
        fontSize: 12,
        color: Color.dim_gray,
        fontFamily: "Comfortaa-Regular",
        alignSelf: 'center',
        marginTop: 12
    },
    plate_number_box: {
        marginTop: 24,
        height: 40,
        width: '100%',
        borderRadius: 3,
        borderWidth: 0.5,
        borderColor: Color.platinum,
        flexDirection: 'row'
    },
    plate_text_box: {
        height: 40,
        backgroundColor: Color.munsell,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 29,
        paddingRight: 29
    },
    plate_text: {
        fontSize: 10,
        color: Color.dim_gray,
        fontFamily: "Comfortaa-Bold"
    },
    plate_input: {
        height: 40,
        flex: 1,
        paddingLeft: 8,
        paddingRight: 8,
        fontSize: 14,
        color: Color.black,
        fontFamily: "Comfortaa-Bold",
        textAlign: 'center'
    },
    submit: {
        marginTop: 24,
        marginBottom: 24,
        height: 40,
        width: '100%',
        backgroundColor: Color.dark_blue,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    btn_text: {
        color: 'white',
        fontSize: 14,
        fontFamily: "Comfortaa-Regular"
    },
    note_text: {
        color: Color.dim_gray,
        fontSize: 12,
        fontFamily: "Comfortaa-Bold",
        alignSelf: 'center'
    },
    title_box: {
        height: 48,
        backgroundColor: Color.dark_blue,
        justifyContent: 'center',
        alignItems: 'center'
    }
});