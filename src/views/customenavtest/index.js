import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Button
} from 'react-native';
import { connect } from 'react-redux';
import { addNavigationHelpers, NavigationActions } from 'react-navigation';
import {
    StackNavigator,
} from 'react-navigation';

class MainScreen extends React.Component {
    render() {
        // const { navigate } = this.props.navigation;
        let { navigate } = this.props.screenProps.rootNavigation;
        return (
            <View>
                {/* <Text>{screenProps.test}dfdsf</Text> */}
                <Button
                    title="Go to Setup Tab"
                    onPress={() => navigate('splash')}
                />
            </View>

        );
    }
}
class SetupScreen extends React.Component {
    render() {
        const { goBack } = this.props.navigation;
        let { test, screenProps } = this.props;
        let nav = screenProps.getChildIndex();
        console.log(this.props);
        return (
            <View>
                <Text>{screenProps.test} dfdsf +{nav.index}</Text>
                <Button
                    title="Go back to home tab"
                    onPress={() => goBack()}
                />
            </View>

        );
    }
}
const BasicApp = StackNavigator({
    Main: { screen: MainScreen },
    Setup: { screen: SetupScreen, test: 'test' },
}, {
        initialRouteName: 'Main',
        headerMode: 'none',
    });

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModal: false,
            modalView: null,
            isLoading: false,
            modalTransparent: true,
            containerMarginTop: 0,
            isPopupOpen: false,
            popupView: null
        }

    }
    //goBack()
    render() {
        let { nav } = this.props;
        const { goBack } = this.props.navigation;
        let { name } = this.props.navigation.state.params;
        return (
            <View style={{ backgroundColor: 'green', height: "100%" }}>
                <TouchableOpacity onPress={this.changePage.bind(this)}>
                    <Text>Login voi {name} vi tri: {nav.index}</Text>
                </TouchableOpacity>
                <BasicApp
                    ref={(mRef) => this.childNavigation = mRef}
                    screenProps={{ test: 'test', rootNavigation: this.props.navigation, getChildIndex:this.getChildIndex.bind(this)}} />
            </View>

        );
    }
    changePage() {
        let { navigate, dispatch } = this.childNavigation._navigation;
        navigate('Setup');
        // dispatch(NavigationActions.reset({
        //     index: 0,
        //     key: null,
        //     actions: [NavigationActions.navigate({ routeName: 'Setup' })]
        // }))

        console.log(this.childNavigation);
    }
    getChildIndex() {
        if (this.childNavigation && this.childNavigation.state) {
            return this.childNavigation.state.nav;
        }
        return {index:0, routes:[]};
    }
}
function mapStateToProps(state) {

    return {
        nav: state.nav
    };
}
export default connect(mapStateToProps, null)(Login);
