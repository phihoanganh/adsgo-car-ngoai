import React, {
    StyleSheet
} from 'react-native';
import Color from '../../global/colors';

module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: '100%',
        width: '100%',
        paddingRight: 20,
        paddingLeft: 20
    },
    logo: {
        width: 178,
        height: 60,
        alignItems: 'center',
        alignSelf: 'center'
    },
    footer_image: {
        width: '100%',
        alignSelf: 'stretch',
        marginBottom: 10
    },
    footer: {
        position: 'absolute',
        width: '100%',
        bottom: 0,
        paddingRight: 32,
    },
    submit: {
        marginTop: 30,
        height: 40,
        width: '100%',
        backgroundColor: Color.dark_blue,
        borderRadius: 5,
        borderColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn_text: {
        color: 'white',
        fontSize: 14,
        fontFamily: "Comfortaa-Regular",
        // fontWeight: 'bold'
    }
});