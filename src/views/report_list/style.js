import React, {
    StyleSheet
} from 'react-native';
import Color from '../../global/colors'

module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '100%',
        height: '100%'
    },
    title_box: {
        height: 48,
        backgroundColor: Color.dark_blue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title_text: {
        fontSize: 14,
        color: 'white',
        fontFamily: "Comfortaa-Bold"
    },
    row: {
        borderBottomColor: Color.platinum,
        borderBottomWidth: 1,
        marginRight: 24,
        marginLeft: 24,
        marginBottom: 6,
        // paddingTop: 6,
        paddingBottom: 12
    },
    item_title_text: {
        fontSize: 14,
        marginTop: 6,
        color: Color.black,
        fontFamily: "Comfortaa-Bold"
    },
    item_date_text: {
        fontSize: 12,
        color: Color.black,
        fontFamily: "Comfortaa-Regular",
        marginTop: 14
    },
    item_sate_text: {
        fontSize: 12,
        // color: Color.deep_saffron,
        fontFamily: "Comfortaa-Regular",
        marginTop: 14
    },
    submit: {
        marginTop: 14,
        height: 40,
        width: '100%',
        // flex: 1,
        backgroundColor: Color.dark_blue,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    btn_text: {
        fontSize: 14,
        color: 'white',
        fontFamily: "Comfortaa-Regular"
    },
    back_btn: {
        height: 48,
        width: 48,
        position: 'absolute',
        left: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    flat_list_mess: {
        fontSize: 14,
        color: Color.black,
        fontFamily: "Comfortaa-Bold",
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    icon_back: {
        height: 14,
        width: 14
    }
});