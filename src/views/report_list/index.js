import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    ScrollView,
    StatusBar,
    Image,
    Dimensions,
    FlatList
} from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base_views'
import styles from './style'
import { getReportList } from '../../actions/view_action/reportListAction';


class Report extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
        };

    }
    renderBaseContent() {
        let reportList = this.props.reportListPage.report_list
        let getError = this.props.reportListPage.get_report_list_error
        let loadDone = this.props.reportListPage.loadDone

        return (
            <View style={styles.container}>
                <View style={styles.title_box}>
                    <Text style={styles.title_text}>Danh sách yêu cầu hình</Text>
                    <TouchableOpacity
                        onPress={() => this.goBack()}
                        style={styles.back_btn}
                        underlayColor='transparent'>
                        <Image style={styles.icon_back} resizeMode={'contain'} source={require('../../assets/images/icon_back.png')}></Image>
                    </TouchableOpacity>
                </View>
                {getError ?
                    <Text style={styles.flat_list_mess}>Không tải được danh sách</Text> :
                    <FlatList
                        ListHeaderComponent={() => ((reportList.length == 0 && loadDone) &&
                            <Text style={styles.flat_list_mess}>Lịch sử trống</Text>)
                        }
                        data={reportList}
                        renderItem={({ item, index }) => this.renderItems(item, index)}
                        keyExtractor={(item, index) => index}
                    />
                }
            </View>
        );
    }
    renderStatusColor(text) {
        if (text == 'Đã duyệt') return '#009951';
        if (text == 'Từ chối') return '#FD6952';
        if (text == 'Chưa có ảnh') return '#919191';
        if (text == 'Chưa duyệt') return '#F5923B';
    }
    renderItems(item, index) {
        return (
            <View style={[styles.row, index == 0 && { paddingTop: 18 }]}>
                <Text style={styles.item_title_text}>{item.tenChienDich}</Text>
                <Text style={styles.item_date_text}>Ngày yêu cầu: {item.requestDate.split(" ").length > 0 && item.requestDate.split(" ")[0]}</Text>
                <Text style={[styles.item_sate_text, { color: this.renderStatusColor(item.trangThaiReport) }]}>Trạng thái: {item.trangThaiReport}</Text>
                {/* {(item.trangThaiReport == 'Chưa có ảnh') && <TouchableOpacity
                    onPress={() => this.pushPage('report', null)}
                    style={styles.submit}
                    underlayColor='transparent'>
                    <Text style={styles.btn_text}>Xử lý</Text>
                </TouchableOpacity>} */}
            </View>
        );
    }
    componentDidMount() {
        this.props.getReportList(this.props.loginPage.user_info.cmnd, this)
    }
}
function mapStateToProps(state) {
    return {
        reportListPage: state.reportListPage,
        loginPage: state.loginPage
    };
}

export default connect(mapStateToProps, { getReportList })(Report);