import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Image,
    Animated,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import BaseView from '../base_views';
import TextField from '../../widgets/text_field';
import Strings from '../../global/strings';
import Const from '../../global/const';
import Utils from '../../global/utils';
import styles from './style';
import call from 'react-native-phone-call';
import { doLogin, getUserInfo } from '../../actions/view_action/loginAction';

const args = {
    number: '096-6699128', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}

class Login extends BaseView {
    constructor(props) {
        super(props);
        this.state = {
            containerMarginTop: 0,
            logoAnim: new Animated.Value(130),
            fadeAnim: new Animated.Value(0),
            isLoading: false
        }
    }

    renderBaseContent() {
        let { nav } = this.props;
        let { logoAnim, fadeAnim } = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.footer}>
                    <Image style={[styles.footer_image, { height: (Dimensions.get('window').width - 32) * 106 / 660 }]} resizeMode={'contain'} source={require('../../assets/images/footer.png')} ></Image>
                </View>
                <ScrollView>
                    <Animated.View style={[styles.logo_box, { marginTop: logoAnim }]}>
                        <Image style={styles.logo} resizeMode={'contain'} source={require('../../assets/images/logo.png')} ></Image>
                    </Animated.View>

                    <Animated.View style={[styles.form, { opacity: fadeAnim }]}>
                        <TextField
                            ref="tf_username"
                            label={Strings.username_hint}
                        />

                        <TextField
                            ref="tf_password"
                            label={Strings.password_hint}
                            secureText={true}
                        />

                        <TouchableOpacity
                            onPress={this.doLogin.bind(this)}
                            style={styles.submit}
                            underlayColor='transparent'>
                            <Text style={styles.btn_text}>{Strings.login_btn}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => {
                            this.renderPopup(
                                'Quên mật khẩu?',
                                'Vui lòng liên hệ 096.669.9128',
                                require('../../assets/images/icon_forgotpass.png'),
                                'Gọi ngay',
                                1,
                                () => { call(args).catch(console.error) }
                            );
                        }}>
                            <Text style={styles.forgot_pass}>{Strings.forgot_pass}</Text>
                        </TouchableOpacity>
                    </Animated.View>
                </ScrollView>
            </View>
        );
    }
    doLogin() {
        // this.resetPage(0, 'main', null);
        let userName = this.refs.tf_username.getValue();
        let password = this.refs.tf_password.getValue();
        this.props.doLogin(userName, password, this);
    }
    componentDidMount() {
        Animated.parallel([
            Animated.timing(
                this.state.logoAnim,
                {
                    toValue: 90,
                    duration: 1000,
                },
            ),

            Animated.timing(
                this.state.fadeAnim,
                {
                    toValue: 1,
                    duration: 2000,
                }
            )
        ]).start();

        Utils.getValueByKey(Const.LOGIN_INPUT_STORE_ID).then((userInfo) => {
            console.log(userInfo);
            if (userInfo !== '' && userInfo !== undefined && userInfo !== null) {
                // console.log(result.username);
                userInfo = JSON.parse(userInfo);
                this.refs.tf_username.setValue(userInfo.username);
                this.refs.tf_password.setValue(userInfo.password);
                Utils.getValueByKey(Const.ACCESS_TOKEN_STORE_ID).then((result) => {
                    if (result !== '' && result !== undefined && result !== null) {
                        this.props.getUserInfo(userInfo.username, this);
                    }
                });
            }
        });

    }
}
function mapStateToProps(state) {
    return {
        nav: state.nav
    };
}
export default connect(mapStateToProps, { doLogin, getUserInfo })(Login);